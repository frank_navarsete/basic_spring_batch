package com.glitchcube.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


/**
 * Run with CommandLineJobRunner (provided by spring)
 * <p>
 * 1. Build the project: <code>mvn clean package</code>
 * <p>
 * 2. Run the batch: <code>java -cp target/basic-spring-batch-jar.jar org.springframework.batch.core.launch.support.CommandLineJobRunner com.glitchcube.batch.HelloWorldBatchJava helloWorldJavaJob</code>
 * The first part is to add the compiled project to the classpath.
 * Then execute the main-method in CommandLineJobRunner.
 * After give it the context for the batch and the name of the job/batch to run.
 * <p>
 * https://docs.spring.io/spring-batch/trunk/reference/html/configureJob.html#runningJobsFromCommandLine
 */
@Configuration
@EnableBatchProcessing
@ImportResource("classpath:common-context.xml")
public class HelloWorldBatchJava {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Bean(name = "helloWorldJavaJob")
    public Job job(@Qualifier("step1") Step step1) {
        return jobs.get("myJob").start(step1).build();
    }

    @Bean
    public Tasklet tasklet() {
        return (contribution, chunkContext) -> {
            System.out.println("####### Hello world!!!!! #######");
            return RepeatStatus.FINISHED;
        };
    }

    @Bean
    public Step step1(@Qualifier("tasklet") Tasklet tasklet) {
        return steps.get("step2").tasklet(tasklet).build();
    }
}
